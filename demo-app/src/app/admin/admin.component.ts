import { Component, OnInit } from '@angular/core';
import { DonneesService, Individu } from '../donnees.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    individuForm:FormGroup = new FormGroup({
    prenom: new FormControl('', [Validators.required, Validators.maxLength(125)]),
    nom: new FormControl('', [Validators.required,Validators.maxLength(125)]),
    photo: new FormControl('', [Validators.required]),
    //enregistrer: new FormControl(''),
   });
  private phototmp:FormControl= new FormControl('');
  constructor(private donneesService: DonneesService) { }

  ngOnInit(): void {
  }

  enregistrer() {
    let formData: FormData = new FormData();
    const enregister = 'enregistrer';

    for (let elemkey in this.individuForm.value) {
      let elemvalue = this.individuForm.value[elemkey];

        formData.append(elemkey, elemvalue);

    }
    formData.append('photo', this.phototmp.value);
    formData.append(enregister,enregister);

    console.log(this.individuForm.value);
    //console.log(formData);
    this.donneesService.sumbitData('/php/individus.php',formData)
  }

  /*
  * apres chargement, on met le fichier dans un champs temporaire
  * pour garder le blob
  */
  fichierCharge(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.phototmp.setValue(file);

    }
  }

}
