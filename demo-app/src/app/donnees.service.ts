import { Injectable } from '@angular/core';
//ajout http
import { HttpClient, HttpHeaders, HttpClientModule, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DonneesService {
  private baseUrl:string = "http://localhost:4300/S22/demo-app/src/assets"
  individus:Individu[]=[
    { "id": 1, "prenom":"Jean", "nom":"Tremblay" },
    {"id":2, "prenom":"Li","nom":"Ying"}
  ];
  constructor(private httpClient: HttpClient) { }

  getIndividu(id:number) {

    let returnval: any = {};
    for (let indiv of this.individus) {
      if (indiv.id == id) {
        return indiv;// on a trouvé individu cherché
      }
    }
    return returnval;
  }

  sumbitData(formAction:string, formData: FormData) {
    let postUrl:string = this.baseUrl + formAction;
    this.httpClient.post<any>(postUrl, formData).subscribe({
      next: (res) => console.log(res),
      error:(err) => console.log(err),
      complete: () => console.info('complete')
    }
    );
  }

  queryHttp(queryUrl: string,para:any={}) {
    let getUrl: string = this.baseUrl + queryUrl;
    return this.httpClient.get<any>(getUrl,{params:para});
  }

}


export interface Individu{
  id: number;
  prenom: String;
  nom: String;
}
