import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnuaireComponent } from './annuaire/annuaire.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  { path: 'annuaire', component: AnnuaireComponent },
  { path: 'admin', component: AdminComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
