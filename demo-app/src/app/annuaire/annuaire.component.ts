import { Component, OnInit } from '@angular/core';
import { DonneesService, Individu } from '../donnees.service';
import { Router } from "@angular/router";//pour faire redirection

@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.component.html',
  styleUrls: ['./annuaire.component.css']
})
export class AnnuaireComponent implements OnInit {
  personnes: any[] = [];
  qerror:any= {};
  qcomplete: number = 0;
  constructor(private donneesService: DonneesService,
    private router: Router //redirection
  ) { }

  ngOnInit(): void {
    //this.personnes = this.donneesService.individus;
    this.getUsersLists();
  }

  getUsersLists(id:number=0) {
    this.personnes = [];
    const qurl: string = '/php/individus.php';
    const qparams = { 'id': id };
    this.donneesService.queryHttp(qurl,qparams).subscribe({
      next: (data: any) => { console.log(data); this.personnes = data },
      error: (err: any) => { console.info(err); this.qerror = err },
      complete: () => {
        console.info("completed"); this.qcomplete = 1;
       // this.router.navigateByUrl("/admin"); // redirection vers admin
      }

    });

    //  (data: any) => { this.personnes = data });
    // this.donneesService.getNotes().subscribe((data: NoteInfo[]) => {this.notes=data });
 }
}
