<?php
define('METHOD_POST','POST',true);
define('METHOD_GET','GET',true);
define('IMAGES_DIR',__DIR__.'/../images');
//require 'head.php';
require 'lib/Medoo.php'; // https://medoo.in/
use Medoo\Medoo;
/*
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'tp',
    'server' => 'db',
    'username' => 'etudiant',
    'password' => 'INF3190pass',
    'prefix' => 's22_'
]);
*/
require 'config.php';//||die("Erreur config.php");
// pour des raisons de securité on met les infos authentification dans un fichier separé
//ce fichier ne doit PAS aller sur le depot git.
if(!$CFG){
  $msg=array('erreur'=>'Erreur importation config.php');
  die(json_encode($msg));
}
$database = new Medoo($CFG);


$method=$_SERVER['REQUEST_METHOD'];

if(strcasecmp($method,METHOD_POST)==0){
  //méthode post

  if($_REQUEST['enregistrer']){
    enregistrer();// enregistrement info individu
    //echo json_encode($_REQUEST);
  }
}
elseif(strcasecmp($method,METHOD_GET)==0){
  //méthode get

  if(isset($_REQUEST['id'])){
    $id=$_REQUEST['id'];
    lire(intval($id));

  }else{
    $msg=array('error'=>'Requette indeterminee');
    die(json_encode($msg));
  }
}


function enregistrer(){
global $database;
$record=array();
$record["prenom"]=$_REQUEST['prenom'];
$record["nom"]=$_REQUEST['nom'];


if($_FILES["photo"]){
  $file=$_FILES["photo"];
  $filename=$file['name'];
  $file_tmp=$file['tmp_name'];
  $contenthash=sha1_file("$file_tmp");
  $record["photo"]="$filename";
  $record["contenthash"]="$contenthash";
  $timestamp=time();
  $record['timecreated']=$timestamp;
  $record['timemodified']=$timestamp;

  if(!file_exists(IMAGES_DIR)){
    if(!mkdir(IMAGES_DIR,0755)){
        $msg=array('erreur'=>'dossier image inexistant');
        die(json_encode($msg));
    }
  }
  $file_to=IMAGES_DIR."/$contenthash";
  if(!move_uploaded_file($file_tmp,$file_to)){
    $msg=array('erreur'=>'Fichier non déplacé dans le dossier images','file_tmp'=>$file_tmp,'file_to'=>$file_to);
        die(json_encode($msg));

  }
  //maintenant on doit enregistrer
  $database->insert("individus",$record);
  if($database->id()){
    $msg=array('message'=>'succès');
   echo json_encode($msg);

  }else{
    $msg=array('erreur'=>'Fichier non déplacé dans le dossier images');
  echo json_encode($fichier);
  }


}
}

function lire(int $id=0){
 global $database;
  $fields= [
     "id",
     "prenom",
     "nom",
     "photo",
     "contenthash",
    "timecreated",
    "timemodified"
  ];
  $where=[];

if($id>0){
  $where['id[=]']=$id;//un individu specific
}
$records=$database->select("individus",$fields,$where);
echo json_encode($records);
}

?>
